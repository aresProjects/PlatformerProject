// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPCharacter.generated.h"

UCLASS()
class PLATFORMERPROJECT_API ATPCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATPCharacter();
	
	// Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
	float ArmLenght;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CameraSpeed;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
	FRotator RotationRate;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USpringArmComponent* CameraArm;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UCameraComponent* Camera;
	
	// Jump
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Jump")
	float JumpVelocity;
	
	// Dash
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Dash")
	float DashDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Dash")
	float DashTime;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Dash")
	float DashCooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Dash")
	FTimerHandle TimerHandle;
		
protected:
	// Friction & Gravity
	float DefaultFriction;
	float DefaultGravity;
	
	// Jump
	mutable int JumpCounter;
		
	// Dash
	bool CanDash;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Landed(const FHitResult& Hit) override;
	
	// Camera
	void LookX(float Val);
	void LookY(float Val);
	
	// Movement
	void MoveX(float Val);
	void MoveY(float Val);
	
	// Jump
	void JumpHandler();
	
	// Dash
	void Dash();
	void StopDash();
	void ResetDash();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};