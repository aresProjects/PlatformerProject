// Fill out your copyright notice in the Description page of Project Settings.


#include "TPCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ATPCharacter::ATPCharacter()
{
 	// Set this character to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Default values: Camera
	ArmLenght = 300.0f;
	RotationRate = FRotator(0, 500.0f, 0);
	CameraSpeed = 50.0f;
	
	// Default value: Jump
	JumpVelocity = 500.0f;
		
	// Default values: Dash
	DashDistance = 2000.0f;
	DashTime = .25f;
	DashCooldown = 1.0f;
	CanDash = true;
	
	// Camera Arm (Boom)
	CameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraArm"));  // Add component
	CameraArm->SetupAttachment(RootComponent);                                   // Attach to "RootComponent"
	CameraArm->TargetArmLength = ArmLenght;	                                     // Set length
	CameraArm->bUsePawnControlRotation = true;                                   // Enable pawn controlled rotation
	
	// Camera
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));           // Add component
	Camera->SetupAttachment(CameraArm, USpringArmComponent::SocketName);         // Attach to "cameraBoom" (at the end of the spring arm)
	Camera->bUsePawnControlRotation = false;                                     // Disable pawn controlled rotation
	
	// Disable mesh rotation
	bUseControllerRotationPitch = bUseControllerRotationRoll = bUseControllerRotationYaw = false;
	
	// Change mesh by movement input
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = RotationRate;
}

// Called when the game starts or when spawned
void ATPCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	// Default values: Friction/Gravity
	DefaultFriction = GetCharacterMovement()->BrakingFrictionFactor;
	DefaultGravity = GetCharacterMovement()->GravityScale;
}

// Called every frame
void ATPCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ATPCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	// Camera
	PlayerInputComponent->BindAxis("LookX", this, &ATPCharacter::LookX);
	PlayerInputComponent->BindAxis("LookY", this, &ATPCharacter::LookY);
	
	// Move
	PlayerInputComponent->BindAxis("MoveX", this, &ATPCharacter::MoveX);
	PlayerInputComponent->BindAxis("MoveY", this, &ATPCharacter::MoveY);
	
	// Jump
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ATPCharacter::JumpHandler);
	
	// Dash
	PlayerInputComponent->BindAction("Dash", IE_Pressed, this, &ATPCharacter::Dash);
}

void ATPCharacter::LookX(const float Val)
{
	AddControllerYawInput(Val * (CameraSpeed * GetWorld()->GetDeltaSeconds()));
}

void ATPCharacter::LookY(const float Val)
{
	AddControllerPitchInput(Val * (CameraSpeed * GetWorld()->GetDeltaSeconds()));
}

void ATPCharacter::MoveX(const float Val)
{
	if (Controller == nullptr || Val == 0.0f) return;
	
	const FRotator& Rotation = Controller->GetControlRotation();	 // Returns controller rotation
	const FRotator Yaw = FRotator(0, Rotation.Yaw, 0);	 // Save yaw rotation
	const FVector& Dir = FRotationMatrix(Yaw).GetUnitAxis(EAxis::Y); // Get direction (normalized)
	
	// Apply movement
	AddMovementInput(Dir, Val);
}

void ATPCharacter::MoveY(const float Val)
{
	if (Controller == nullptr || Val == 0.0f) return;
	
	const FRotator& Rotation = Controller->GetControlRotation();	 // Returns controller rotation
	const FRotator Yaw = FRotator(0, Rotation.Yaw, 0);	 // Save yaw rotation
	const FVector& Dir = FRotationMatrix(Yaw).GetUnitAxis(EAxis::X); // Get direction (normalized)
	
	// Apply movement
	AddMovementInput(Dir, Val);
}

void ATPCharacter::JumpHandler()
{
	// Double jump
	if (JumpCounter < 2)
	{
		// Check if player is grounded (to execute the first jump)
		if (!GetCharacterMovement()->IsMovingOnGround() && JumpCounter == 0) return;
		
		FVector jumpVector = FVector(0, 0, JumpVelocity);
		LaunchCharacter(jumpVector, false, true);
		JumpCounter++;
	}
}

void ATPCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	
	// Reset counter
	JumpCounter = 0;
}

void ATPCharacter::Dash()
{
	if (!CanDash) return;
	
	// Disable friction and gravity
	GetCharacterMovement()->BrakingFrictionFactor = 0.0f;
	GetCharacterMovement()->GravityScale = 0.0f;
	
	// Dash vector
	FVector&& cameraDir = Camera->GetForwardVector().GetSafeNormal();
	const FVector dashVector = FVector(cameraDir.X, cameraDir.Y, 0) * DashDistance;
	
	// Rotate character to dash dir
	FRotator&& cameraRot = Camera->GetComponentRotation();
	const FRotator targetRot(0, cameraRot.Yaw, 0);
	SetActorRotation(targetRot);
	
	// Apply dash
	LaunchCharacter(dashVector, false, false);
	
	// Update CanDash
	CanDash = false;
	
	// Set timer
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATPCharacter::StopDash, DashTime, false);
}

void ATPCharacter::StopDash()
{
	// Stop dash
	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->BrakingFrictionFactor = DefaultFriction;
	GetCharacterMovement()->GravityScale = DefaultGravity;
	
	// Set timer
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATPCharacter::ResetDash, DashCooldown, false);
}

void ATPCharacter::ResetDash()
{
	CanDash = true;
}